package com.kshrd.mp.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kshrd.mp.R;
import com.kshrd.mp.data.model.Article;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder> {

    List<Article> articles;
    Context context;
    GetArticle getArticle;

    public ArticleAdapter(Context context, List<Article> articles, GetArticle getArticle) {
        this.articles = articles;
        this.context = context;
        this.getArticle = getArticle;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public List<Article> articles() {
        return articles;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.title.setText(articles.get(position).getTitle());
        if (!articles.get(position).getImageUrl().equals(null)) {
            Glide.with(context)
                    .load(articles.get(position).getImageUrl())
                    .placeholder(R.drawable.forest)
                    .into(holder.imageView);
        } else if (articles.get(position).getImageUrl().equals(null)) {
            Glide.with(context)
                    .load(R.drawable.forest)
                    .placeholder(R.drawable.forest)
                    .into(holder.imageView);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getArticle.onItemClick(articles.get(position));
            }
        });

        holder.actionPupup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupMenu popupMenu = new PopupMenu(context, holder.actionPupup);
                popupMenu.inflate(R.menu.menu_popup);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @SuppressLint("NonConstantResourceId")
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.edit:
                                getArticle.onItemUpdate(articles.get(position));
                                break;
                            case R.id.remove:
                                getArticle.onItemDelete(articles.get(position));
                                notifyDataSetChanged();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public interface GetArticle {
        void onItemClick(Article article);

        void onItemDelete(Article article);

        void onItemUpdate(Article article);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView imageView, actionPupup;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.item_title);
            imageView = itemView.findViewById(R.id.item_image);
            actionPupup = itemView.findViewById(R.id.action_popup);
        }
    }
}
