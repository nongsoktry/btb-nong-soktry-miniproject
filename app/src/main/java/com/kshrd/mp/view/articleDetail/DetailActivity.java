package com.kshrd.mp.view.articleDetail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.kshrd.mp.R;
import com.kshrd.mp.data.model.Article;

public class DetailActivity extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView title = findViewById(R.id.detail_title);
        TextView desc = findViewById(R.id.detail_desc);
        TextView date = findViewById(R.id.detail_date);
        ImageView image = findViewById(R.id.detail_image);

        Intent i = getIntent();
        Article article = (Article) i.getSerializableExtra("detail");

        Glide.with(getApplicationContext()).load(article.getImageUrl())
                .placeholder(R.drawable.forest)
                .into(image);

        title.setText("Title: " + article.getTitle());
        desc.setText("Desc: " + article.getDesc());

        String year = article.getCreatedDate().substring(0, 4);
        String month = article.getCreatedDate().substring(4, 6).concat("/");
        String day = article.getCreatedDate().substring(6, 8).concat("/");
        date.setText("Date: " + day + month + year);

    }

}
