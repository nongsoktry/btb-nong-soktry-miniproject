package com.kshrd.mp.view.articleUpdate;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.kshrd.mp.R;
import com.kshrd.mp.data.model.Article;
import com.kshrd.mp.data.model.response.ImageResponse;
import com.kshrd.mp.data.model.UploadArticle;
import com.kshrd.mp.data.model.response.UpdateResponse;
import com.kshrd.mp.view.MainActivity;
import com.kshrd.mp.viewmodel.ViewModel;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateActivity extends AppCompatActivity {

    EditText title, desc, date;
    ImageView image;
    Button cancel, save;
    Article article = null;
    ImageResponse imageResponse = null;
    ViewModel viewModel;
    Uri uri;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        Intent intent = getIntent();
        article = (Article) intent.getSerializableExtra("update");

        title = findViewById(R.id.upload_title);
        desc = findViewById(R.id.upload_desc);
        date = findViewById(R.id.upload_date);
        image = findViewById(R.id.upload_image);
        save = findViewById(R.id.save_button);
        cancel = findViewById(R.id.cancel_button);

        viewModel = ViewModelProviders.of(this).get(ViewModel.class);
        viewModel.init();

        title.setText(article.getTitle());
        desc.setText(article.getDesc());
        Glide.with(getApplicationContext()).load(article.getImageUrl()).placeholder(R.drawable.forest).into(image);

        date.setText("Just cant update date!");
        date.setEnabled(false);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int id = article.getId();
                String setTitle = title.getText().toString();
                String setDesc = desc.getText().toString();
                if (!title.toString().isEmpty() && !setDesc.isEmpty()) {

                    UploadArticle uploadArticle = new UploadArticle();
                    uploadArticle.setDesc(setDesc);
                    uploadArticle.setTitle(setTitle);

                    if (imageResponse != null) {
                        uploadArticle.setImage(imageResponse.getImage());
                        Glide.with(UpdateActivity.this).load(imageResponse.getImage()).into(image);

                        viewModel.setId(id);
                        viewModel.uploadArticle(uploadArticle);
                        viewModel.updateArticle();
                        viewModel.getUpdateArticle().observe(UpdateActivity.this, new Observer<UpdateResponse>() {
                            @Override
                            public void onChanged(UpdateResponse updateResponse) {
                                viewModel.getArticles();
                            }
                        });
                        Intent intent = new Intent(UpdateActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else if (imageResponse == null) {
                        Toast.makeText(getApplicationContext(), "Please add an image and \nwait for a moment!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please wait till image is uploaded successfully!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please input title and desc correctly!", Toast.LENGTH_LONG).show();
                }
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            Intent intentImage = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intentImage, 1);
                        } catch (Exception ignored) {
                        }

                    }
                });
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            uri = data.getData();
            image.setImageURI(uri);

            File file = new File(getPath(uri));

            RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

            viewModel.setFile(file);
            viewModel.setFileBody(requestBody);
            viewModel.setImage(body);
            viewModel.uploadImage();
            viewModel.getUploadImage().observe(this, new Observer<ImageResponse>() {
                @Override
                public void onChanged(ImageResponse imageResponse) {
                    UpdateActivity.this.imageResponse = imageResponse;
                    Toast.makeText(UpdateActivity.this, "Uploaded image successfully!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }

}
