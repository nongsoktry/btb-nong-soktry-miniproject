package com.kshrd.mp.data.model.response;

import com.google.gson.annotations.SerializedName;

public class ImageResponse {

    @SerializedName("data")
    private String data;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;

    public ImageResponse() {
    }

    public ImageResponse(String data, String message, int code) {
        this.data = data;
        this.message = message;
        this.code = code;
    }

    public String getImage() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "ServerResponseImage{" +
                "data='" + data + '\'' +
                ", message='" + message + '\'' +
                ", code=" + code +
                '}';
    }
}
