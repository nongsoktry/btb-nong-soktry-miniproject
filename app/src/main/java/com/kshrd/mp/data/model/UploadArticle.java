package com.kshrd.mp.data.model;

import com.google.gson.annotations.SerializedName;

public class UploadArticle {

    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("status")
    private String status;
    @SerializedName("description")
    private String desc;

    @Override
    public String toString() {
        return "UploadArticle{" +
                ", image='" + image + '\'' +
                ", status='" + status + '\'' +
                ", description='" + desc + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

    public UploadArticle() {
    }

    public UploadArticle(String image, String status, String desc, String title) {
        this.image = image;
        this.status = status;
        this.desc = desc;
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
